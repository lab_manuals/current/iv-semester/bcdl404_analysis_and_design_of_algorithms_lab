/***************************************************************************
Compiling instructions
$ g++ 03AFloyd.cpp -o FLOYD.x
Execution
$./FLOYD.x

SAMPLE 1
*********************************************************
*	PROGRAM TO IMPLEMENT FLOYD'S ALGORITHM	*
*********************************************************
Enter the number of vertices
5

Enter the Cost adjacency Matrix
0 3 9999 7 9
3 0 4 2 9999
9999 4 0 5 6
7 2 5 0 4
9 9999 6 4 0

Input Graph
0	3	9999	7	9	
3	0	4	2	9999	
9999	4	0	5	6	
7	2	5	0	4	
9	9999	6	4	0	


All Pair Shortest Path Matrix
0	3	7	5	9	
3	0	4	2	6	
7	4	0	5	6	
5	2	5	0	4	
9	6	6	4	0	

SAMPLE 2
Enter the number of vertices
6

Enter the Cost adjacency Matrix
0		9999	20		9999	9999	9999	
15		0		9999	10		9999	9999
10		15		0		9999	9999	9999
9999	9999	20		0		30		4
45		20		9999	35		0		9999
9999	9999	9999	9999	9999	0

Input Graph
0	9999	20	9999	9999	9999	
15	0	9999	10	9999	9999	
10	15	0	9999	9999	9999	
9999	9999	20	0	30	4	
45	20	9999	35	0	9999	
9999	9999	9999	9999	9999	0	


All Pair Shortest Path Matrix
0		35		20		45		75		49	
15		0		30		10		40		14	
10		15		0		25		55		29	
30		35		20		0		30		4	
35		20		50		30		0		34	
9999	9999	9999	9999	9999	0	

SAMPLE 3
Enter the number of vertices
4

Enter the Cost adjacency Matrix
0		9999	3		9999
2		0		9999	9999
9999	7		0		1
6		9999	9999	0

Input Graph
0		9999	3		9999	
2		0		9999	9999	
9999	7		0		1	
6		9999	9999	0	


All Pair Shortest Path Matrix
0	10	3	4	
2	0	5	6	
7	7	0	1	
6	16	9	0	
***************************************************************************/

