/******************************************************************************
Enter the number of nodes
5
Enter the Cost Matrix

0		3		9999	7		9999
3		0		4		2		9999
9999	4		0		5		6
7		2		5		0		4
9999	9999	6		4		0

//For Source Vertex : 0 shortest path to other vertices//

0<--0 = 0
1<--0 = 3
2<--1<--0 = 7
3<--1<--0 = 5
4<--3<--1<--0 = 9
Press Enter to continue... 

//For Source Vertex : 1 shortest path to other vertices//

0<--1 = 3
1<--1 = 0
2<--1 = 4
3<--1 = 2
4<--3<--1 = 6
Press Enter to continue...
//For Source Vertex : 2 shortest path to other vertices//

0<--1<--2 = 7
1<--2 = 4
2<--2 = 0
3<--2 = 5
4<--2 = 6
Press Enter to continue...

//For Source Vertex : 3 shortest path to other vertices//

0<--1<--3 = 5
1<--3 = 2
2<--3 = 5
3<--3 = 0
4<--3 = 4
Press Enter to continue...

//For Source Vertex : 4 shortest path to other vertices//

0<--1<--3<--4 = 9
1<--3<--4 = 6
2<--4 = 6
3<--4 = 4
4<--4 = 0
******************************************************************************/
