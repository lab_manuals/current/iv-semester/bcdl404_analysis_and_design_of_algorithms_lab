/********************************************************************************
*File		: 07GreedyKnapsack.cpp
*Description: Program to find solution to discrete & continous Knapsack problem using Greedy Method
*Author		: Prabodh C P
*Compiler	: g++ compiler 11.4.0, Ubuntu 22.04
*Date		: 31 Mar 2024
********************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cstdlib>
using namespace std;


typedef struct{
    int profit;
    int weight;
    float profitRatio;
    float fraction;
}Item;
int main()
{
    vector <Item> itemList;
    int iNum, i, iPos;
    float knCap, remCap, totProfit;
    Item a;

    cout << "Enter the number of items : " ;
    cin >> iNum;

    cout << "Enter Knapsack capacity : ";
    cin >> knCap;

	//for each item calculate the profit ratio and add that item to the item list in sorted order of profits
    for(i=0;i<iNum;i++)
    {
        cout << "Enter profit : "; cin >> a.profit;
        cout << "Enter weight : "; cin >> a.weight;
        a.profitRatio = (float)a.profit / a.weight;
        a.fraction = 0.0f;
        //cout << a.profitRatio << endl;
        if (itemList.size() == 0)
        {
            itemList.push_back(a);
        }
        else
        {
            iPos=0;
            while(a.profitRatio < itemList[iPos].profitRatio) iPos++;
            itemList.insert(itemList.begin() + iPos,a);
        }
    }

    remCap = knCap;
    totProfit = 0.0;

    for(i=0;i<iNum;i++)
    {
        a = itemList[i];
        if(a.weight < remCap)
        {
            itemList[i].fraction = 1.0;
            remCap -= itemList[i].weight;
            totProfit += itemList[i].profit;
        }

        if(remCap == 0)
            break;
    }
    cout << "\nDISCRETE KNAPSACK GREEDY SOLUTION\n";
    cout << "Item\tWeight\tProfit\tFraction_Chosen\n";
    for(i=0;i<iNum;i++)
    {
        cout <<setw(4) << i+1 << "\t" << setw(6) << itemList[i].weight << "\t" << setw(6) << itemList[i].profit << "\t" << setw(6) << setprecision(2) << itemList[i].fraction << endl;
    }
    cout  << "\nTotal Profit Earned : " << fixed << totProfit << endl;

    remCap = knCap;
    totProfit = 0.0;

    for(i=0;i<iNum;i++)
    {
    	itemList[i].fraction = 0.0f;
    }    

    for(i=0;i<iNum;i++)
    {
        a = itemList[i];
        if(a.weight < remCap)
        {
            itemList[i].fraction = 1.0;
            remCap -= itemList[i].weight;
            totProfit += itemList[i].profit;
        }
        else
        {
            itemList[i].fraction = remCap / itemList[i].weight;
            remCap -= itemList[i].weight * itemList[i].fraction;
            totProfit += itemList[i].profit * itemList[i].fraction;
        }
        if(remCap == 0)
            break;
    }
    cout << "\nCONTINUOUS KNAPSACK GREEDY SOLUTION\n";
    cout << "Item\tWeight\tProfit\tFraction Chosen\n";
    for(i=0;i<iNum;i++)
    {
        cout <<setw(4) << i+1 << "\t" << setw(6) << itemList[i].weight << "\t" << setw(6) << itemList[i].profit << "\t" << setw(6) << setprecision(2) << itemList[i].fraction << endl;
    }
    cout  << "\nTotal Profit Earned : " << fixed << totProfit << endl;

    return 0;
}

